package com.jaspreet.wolfmanga.ui.home.frg.home

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.jaspreet.wolfmanga.domain.GetCategoryDataUseCase
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class HomeViewModelFactory  @Inject constructor(
    private val getCategoryDataUseCase: GetCategoryDataUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {

            return HomeViewModel(getCategoryDataUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}