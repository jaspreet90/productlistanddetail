package com.jaspreet.wolfmanga.data

data class MainCategoryModelUI(
    var title: String?,
    var category: String?,
    var primaryImage: String?,
    var secondaryImage: String?,
    var type: Int,
    var itemCount: Int?
) {

    companion object {
        val HEADING_TYPE = 0
        val DESC_TYPE = 1
    }
}