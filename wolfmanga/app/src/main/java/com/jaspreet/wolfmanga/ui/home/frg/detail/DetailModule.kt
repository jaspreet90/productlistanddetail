package com.jaspreet.wolfmanga.ui.home.frg.detail

import android.arch.lifecycle.ViewModelProvider
import com.jaspreet.wolfmanga.ui.home.HomeActivity
import dagger.Module
import dagger.Provides

@Module
class  DetailModule {

    @Provides
    fun provideViewModel(homeActivity: HomeActivity, detailViewModelFactory: DetailViewModelFactory) = ViewModelProvider(homeActivity, detailViewModelFactory).get(  DetailViewModel::class.java)
}
