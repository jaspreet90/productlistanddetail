package com.jaspreet.wolfmanga.ui.home.frg.detail

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.google.gson.Gson
import com.jaspreet.wolfmanga.domain.GetCategoryDetailUseCase
import com.jaspreet.wolfmanga.repo.network.model.particularcategory.Data
import com.jaspreet.wolfmanga.repo.network.model.particularcategory.ParticularCategoryResponse
import com.jaspreet.wolfmanga.ui.base.BaseViewModel

class DetailViewModel(private val categoryDetailUseCase: GetCategoryDetailUseCase ) : BaseViewModel() {

    var detailLiveData = MutableLiveData<Data>()

    fun getDetail(){
        addDisposable( categoryDetailUseCase.getDetail()
             .map {
                 var res= Gson().fromJson(it,ParticularCategoryResponse::class.java)
                 return@map  res.data
            }
            .subscribe({
                Log.d("==detail Cat Response=", Gson().toJson(it))
                detailLiveData.postValue(it)
            },{
                Log.d("== error=", it.message)
            })
        )
    }
}