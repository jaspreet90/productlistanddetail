package com.jaspreet.wolfmanga.ui.home.frg.home

import android.arch.lifecycle.ViewModelProvider
import com.jaspreet.wolfmanga.ui.home.HomeActivity
import dagger.Module
import dagger.Provides

@Module
class HomeModule {

    @Provides
    fun provideViewModel(homeActivity: HomeActivity, homeViewModelFactory: HomeViewModelFactory) = ViewModelProvider(homeActivity, homeViewModelFactory).get(  HomeViewModel::class.java)
}
