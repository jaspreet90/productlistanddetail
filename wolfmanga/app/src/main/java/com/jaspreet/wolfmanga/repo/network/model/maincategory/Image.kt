package com.jaspreet.wolfmanga.repo.network.model.maincategory

import com.google.gson.annotations.SerializedName

data class Image(
    @SerializedName("mobile")
    val mobile: String,
    @SerializedName("placeholder")
    val placeholder: String
)