package com.jaspreet.wolfmanga.ui.home.frg.home

import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.jaspreet.wolfmanga.R
import com.jaspreet.wolfmanga.data.MainCategoryModelUI
import com.jaspreet.wolfmanga.listener.ItemClick
import com.jaspreet.wolfmanga.ui.base.BaseFragment
import com.jaspreet.wolfmanga.ui.home.adapter.HomeAdapter
import com.jaspreet.wolfmanga.ui.home.frg.detail.DetailFrg
import kotlinx.android.synthetic.main.frg_home.*
import javax.inject.Inject
import android.view.MenuInflater




open class HomeFrg: BaseFragment<HomeViewModel>() , ItemClick {


    companion object{
          fun newIntance() = HomeFrg()
    }

    private var mAdapter: HomeAdapter? = null
    @Inject
    lateinit var homeViewModel: HomeViewModel

    override fun getViewModel(): HomeViewModel = homeViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onResume() {
        super.onResume()
        setToolbar()
    }

    fun setToolbar(){
        (activity as AppCompatActivity).supportActionBar?.title=getString(R.string.app_name)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.frg_home,container,false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpRecyclerView()
        homeViewModel.getMainCategoryList()
        bindViewModel()

    }

    private fun bindViewModel(){

        homeViewModel.mainCategoryDataLiveData.observe(this, Observer {
            mAdapter?.mainCategoryList = it ?: emptyList()
        })
    }




    fun setUpRecyclerView(){
        var linearLayoutManager= LinearLayoutManager(requireContext())
        rv_cat.layoutManager = linearLayoutManager
        mAdapter = HomeAdapter(requireContext(), this)
        rv_cat.adapter = mAdapter
    }


    override fun onItemClick(mainCategoryModelUI: MainCategoryModelUI) {
         changeFrg(DetailFrg.newInstance(),true)

     }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {

        inflater?.inflate(R.menu.main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}

