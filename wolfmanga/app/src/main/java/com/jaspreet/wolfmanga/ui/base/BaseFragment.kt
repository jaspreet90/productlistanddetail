package com.jaspreet.wolfmanga.ui.base

import android.Manifest
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import dagger.android.support.AndroidSupportInjection
import java.io.ByteArrayOutputStream

abstract class BaseFragment<out VM : BaseViewModel> : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (hasInjector()) {
            AndroidSupportInjection.inject(this)
        }

    }


    open fun hasInjector() = true

    abstract fun getViewModel(): VM

    fun changeFrg(frg:Fragment, isAddToBackStack:Boolean){
        (requireActivity() as BaseActivity).changeFragment(frg, isAddToBackStack)
    }

    fun getImageUri( inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.WEBP, 100, bytes)
        val path = MediaStore.Images.Media.insertImage( requireActivity().contentResolver, inImage, "", null)
        return Uri.parse(path)
    }


}