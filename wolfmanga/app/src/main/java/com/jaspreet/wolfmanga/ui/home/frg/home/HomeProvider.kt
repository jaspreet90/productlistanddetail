package com.jaspreet.wolfmanga.ui.home.frg.home
 import com.jaspreet.wolfmanga.ui.home.frg.home.HomeFrg
 import com.jaspreet.wolfmanga.ui.home.frg.home.HomeModule
 import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeProvider {

    @ContributesAndroidInjector(modules = [(HomeModule::class)])
    abstract fun bindFrg(): HomeFrg
}