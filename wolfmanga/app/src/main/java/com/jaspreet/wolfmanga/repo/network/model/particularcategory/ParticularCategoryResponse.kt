package com.jaspreet.wolfmanga.repo.network.model.particularcategory

import com.google.gson.annotations.SerializedName

data class ParticularCategoryResponse(
    @SerializedName("data")
    val `data`: Data
)