package com.jaspreet.wolfmanga.ui.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.jaspreet.wolfmanga.R
import dagger.android.AndroidInjection

abstract class BaseActivity: AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        if (hasInjector())
            AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

    }


    open fun hasInjector() = true

    fun changeFragment(frg:Fragment , isAddToBackStack:Boolean){
       var frgTransition= supportFragmentManager.beginTransaction()
            .replace(R.id.fl_container,frg)

        if (isAddToBackStack){
            frgTransition.addToBackStack(null)
        }
        frgTransition.commit()
     }

}