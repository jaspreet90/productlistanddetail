package com.jaspreet.wolfmanga.repo.network.model.maincategory

import com.google.gson.annotations.SerializedName

data class Item(
    @SerializedName("category")
    val category: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("image")
    val image: Image,
    @SerializedName("itemCount")
    val itemCount: Int,
    @SerializedName("slug")
    val slug: String
)