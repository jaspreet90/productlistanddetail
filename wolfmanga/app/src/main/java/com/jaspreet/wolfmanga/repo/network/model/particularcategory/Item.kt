package com.jaspreet.wolfmanga.repo.network.model.particularcategory

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Item(
    @SerializedName("image")
    val image: Image,
    @SerializedName("price")
    val price: Int,
    @SerializedName("slug")
    val slug: String,
    @SerializedName("title")
    val title: String
) :  Parcelable {
    constructor(source: Parcel) : this(
        source.readParcelable<Image>(Image::class.java.classLoader),
        source.readInt(),
        source.readString(),
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(image, 0)
        writeInt(price)
        writeString(slug)
        writeString(title)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Item> = object : Parcelable.Creator<Item> {
            override fun createFromParcel(source: Parcel): Item = Item(source)
            override fun newArray(size: Int): Array<Item?> = arrayOfNulls(size)
        }
    }
}