package com.jaspreet.wolfmanga.readassets

import io.reactivex.Flowable

interface ReadAsset {

    fun read(fileName :String) : Flowable<String>
}