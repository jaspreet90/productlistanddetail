package com.jaspreet.wolfmanga.ui.home.frg.detail

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import javax.inject.Inject
import com.jaspreet.wolfmanga.repo.network.model.particularcategory.Item
import com.jaspreet.wolfmanga.ui.home.adapter.DetailGalleryPagerAdapter
import com.jaspreet.wolfmanga.ui.home.frg.bottomsheet.BottomSheetFragment
import kotlinx.android.synthetic.main.detail_frg.*
import java.util.ArrayList
import android.support.v7.app.AppCompatActivity
import com.jaspreet.wolfmanga.R
import android.content.Intent
import android.widget.Toast
import com.bumptech.glide.Glide
import android.content.ActivityNotFoundException
import android.graphics.Bitmap
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.transition.Transition
import com.jaspreet.wolfmanga.ui.base.PermissionBaseFragment

class DetailFrg : PermissionBaseFragment<DetailViewModel>() {
    override fun permissionGranted() {
       shareData()
    }

    override fun permissionDenied() {
        Toast.makeText(requireContext(),"Permission Denied", Toast.LENGTH_SHORT).show()
     }

    var selectedPagerPosition=0

    @Inject lateinit var detailViewModel: DetailViewModel

    lateinit var detailGalleryPagerAdapter: DetailGalleryPagerAdapter

    var listItem=ArrayList<Item>()

    override fun getViewModel(): DetailViewModel  = detailViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?  =
     inflater.inflate(R.layout.detail_frg,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUp()
        detailViewModel.getDetail()
        bindViewModel()
    }

    fun setUp(){
        setToolbar()

        detailGalleryPagerAdapter =   DetailGalleryPagerAdapter(childFragmentManager)
        pager.adapter = detailGalleryPagerAdapter


        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(position: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(position: Int) {
                selectedPagerPosition=position
                setUpBottomSheet(selectedPagerPosition)
            }
        })
        setUpBottomSheet(selectedPagerPosition)

        fab_whats_app.setOnClickListener {
            shareInfo()
        }
    }

    fun setUpBottomSheet(position: Int) {
        btn_bottom_sheet.setOnClickListener {
            showBottomSheetDialogFragment(position)
        }
    }

    private fun showBottomSheetDialogFragment(position: Int) {
        val bottomSheetFragment = BottomSheetFragment.newInstance(position, listItem)
         bottomSheetFragment.show(childFragmentManager, bottomSheetFragment.tag)
    }

    private fun bindViewModel(){
        detailViewModel.detailLiveData.observe(this, Observer {
            setUpToolbarTitle(it?.category)
            listItem =it?.items as ArrayList<Item>
            detailGalleryPagerAdapter.listItem= listItem

        })
    }

    fun setUpToolbarTitle(category: String?) {
        (activity as AppCompatActivity).supportActionBar?.title= category.toString()
    }
    fun setToolbar(){
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }



    private fun shareInfo() {

        checkPermission()

    }

    fun shareData(){
        Glide.with(requireActivity())
            .asBitmap()
            .load(listItem[selectedPagerPosition].image.mobile)
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
            .into(object : SimpleTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {

                    var imageUri=  getImageUri(resource)
                    val waIntent = Intent(Intent.ACTION_SEND)
                    waIntent.type = "image/*"
                    waIntent.setPackage("com.whatsapp")
                    waIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
                    waIntent.putExtra(Intent.EXTRA_TEXT, listItem[selectedPagerPosition].title)


                    try {
                        startActivity(waIntent)
                    } catch (ex: ActivityNotFoundException) {
                        Toast.makeText(requireContext(),"Whatsapp have not been installed.", Toast.LENGTH_SHORT).show()
                    }
                }
            })
    }

     companion object{
        fun newInstance() = DetailFrg()
    }
}