package com.jaspreet.wolfmanga.ui.home.adapter

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.jaspreet.wolfmanga.R
import com.jaspreet.wolfmanga.data.MainCategoryModelUI
import com.jaspreet.wolfmanga.listener.ItemClick
import java.lang.Exception


class HomeAdapter (var context: Context, private val itemClick: ItemClick) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mainCategoryList: List<MainCategoryModelUI> = mutableListOf<MainCategoryModelUI>()
    set(value) {
        field=value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        var view:View

        when (viewType) {
            MainCategoryModelUI.HEADING_TYPE -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_heading, parent, false)
                return HeadingViewHolder(view)
            }
            MainCategoryModelUI.DESC_TYPE -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_data, parent, false)
                return ItemViewHolder(view)
            }
        }
         return throw Exception()
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val obj = mainCategoryList.get(position)
        if (obj != null) {
            when (obj.type) {
                MainCategoryModelUI.HEADING_TYPE -> (holder as HeadingViewHolder).mTitle.text = obj.title

                MainCategoryModelUI.DESC_TYPE -> {
                    (holder as ItemViewHolder).mTitle.text = obj.category
                    holder.mDescription.text = "3 hr ago"
                    holder.count.text=obj.itemCount.toString()

                    Glide.with(context)
                        .load(obj.primaryImage)
                        .centerCrop()
                        .error(R.drawable.ic_launcher_background).into(holder.imageView)

                    holder.cardViewMain.setOnClickListener {
                        Log.d("==click=","cardview")
                        itemClick.onItemClick(obj)
                    }
                }
            }
        }
    }

    class HeadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
          val mTitle: TextView

        init {
            mTitle = itemView.findViewById(R.id.titleTextView) as TextView
        }
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
          val mTitle: TextView
          val mDescription: TextView
          val imageView: ImageView
          val cardViewMain:CardView
          val count:TextView

        init {
            mTitle = itemView.findViewById<View>(R.id.title) as TextView
            mDescription = itemView.findViewById<View>(R.id.tv_time) as TextView
            imageView= itemView.findViewById<View>(R.id.iv_cat) as ImageView
            cardViewMain =itemView.findViewById(R.id.cv_main) as CardView
            count =itemView.findViewById(R.id.tv_count) as TextView
        }
    }

    override fun getItemCount(): Int {
        return if(mainCategoryList==null) 0 else mainCategoryList.size
    }


    override fun getItemViewType(position: Int): Int {
        if (mainCategoryList != null) {
            val obj = mainCategoryList[position]
            if (obj != null) {
                return obj.type
            }
        }
        return 0
    }
}


