package com.jaspreet.wolfmanga.ui.home.frg.home

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.jaspreet.wolfmanga.data.MainCategoryMaptoUIModel
import com.jaspreet.wolfmanga.data.MainCategoryModelUI
import com.jaspreet.wolfmanga.domain.GetCategoryDataUseCase
import com.jaspreet.wolfmanga.ui.base.BaseViewModel

class HomeViewModel(private val getCategoryDataUseCase: GetCategoryDataUseCase) : BaseViewModel() {


     var mainCategoryDataLiveData =MutableLiveData<List<MainCategoryModelUI>>()

     fun getMainCategoryList(){
       addDisposable( getCategoryDataUseCase.getList()
           .map {
               return@map  MainCategoryMaptoUIModel.mapper(it)
           }
          .subscribe({
           Log.d("==main Cat Response=",Gson().toJson(it))
           mainCategoryDataLiveData.postValue(it)

        },{
                Log.d("== error=", it.message)
            })
       )
    }

}