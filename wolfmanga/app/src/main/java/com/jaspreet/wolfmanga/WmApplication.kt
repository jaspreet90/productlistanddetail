package com.jaspreet.wolfmanga

import android.app.Activity
import android.app.Application
import com.jaspreet.wolfmanga.di.component.ApplicationComponent
import com.jaspreet.wolfmanga.di.component.DaggerApplicationComponent
import com.jaspreet.wolfmanga.di.module.ApplicationModule
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.reactivex.plugins.RxJavaPlugins
import javax.inject.Inject

class WmApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    private lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
        applicationComponent.inject(this)

        setupDefaultRxJavaErrorHandler()

    }

    private fun setupDefaultRxJavaErrorHandler() {
        RxJavaPlugins.setErrorHandler { throwable ->
            throwable.printStackTrace()
        }
    }

    override fun activityInjector() = activityInjector

}