package com.jaspreet.wolfmanga.data

import com.google.gson.Gson
import com.jaspreet.wolfmanga.repo.network.model.maincategory.MainCategoryResponse

object MainCategoryMaptoUIModel  {

    fun mapper(it:String) : MutableList<MainCategoryModelUI> {
        var mainCategoryResponse=   Gson().fromJson(it, MainCategoryResponse::class.java)

        var mainCategoryModelUI = mutableListOf<MainCategoryModelUI>()

        for (item in mainCategoryResponse.widgets) {
            mainCategoryModelUI.add(MainCategoryModelUI(
                item.title,
                null,
                null,
                null,
                MainCategoryModelUI.HEADING_TYPE,
                null
            ))

            for(data in item.items){
                mainCategoryModelUI.add(MainCategoryModelUI(null,data.category,data.image.mobile,data.image.placeholder,MainCategoryModelUI.DESC_TYPE , data.itemCount))
            }
        }
        return mainCategoryModelUI
    }
}