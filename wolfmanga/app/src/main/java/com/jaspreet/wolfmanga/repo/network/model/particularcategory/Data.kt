package com.jaspreet.wolfmanga.repo.network.model.particularcategory

import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("category")
    val category: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("items")
    val items: List<Item>,
    @SerializedName("slug")
    val slug: String
)