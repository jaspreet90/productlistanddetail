package com.jaspreet.wolfmanga.ui.home.frg.detail

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jaspreet.wolfmanga.R
import com.jaspreet.wolfmanga.ui.base.BaseFragment
import kotlinx.android.synthetic.main.slide.*
import javax.inject.Inject
import com.bumptech.glide.Glide
import com.jaspreet.wolfmanga.repo.network.model.particularcategory.Item
import java.util.ArrayList
import android.support.design.widget.BottomSheetDialog


class DetailGalleryFrg : BaseFragment<DetailViewModel>() {

    override fun getViewModel(): DetailViewModel  = detailViewModel

    @Inject lateinit var detailViewModel: DetailViewModel

    var position :Int=0
    var listItem = emptyList<Item>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getBundleData()
    }

    fun getBundleData(){
          position = arguments?.getInt(index)?:0
          listItem = arguments?.getParcelableArrayList<Item>(list)?: emptyList()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
       = inflater.inflate(R.layout.slide,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()

    }

    private fun setUp(){
       setUpSlider()

    }

    private fun setUpSlider(){
        var url = listItem[position].image.mobile
        Log.d("==url==",url)

        Glide.with(requireActivity())
            .load(url)
            .centerCrop()
            .placeholder(R.drawable.ic_loading)
            .error(R.drawable.ic_launcher_background).into(image)
    }



    companion object{

        var index="index"
        var list="list"

        fun newInstance(listItem: ArrayList<Item>, position: Int ) : DetailGalleryFrg {

            var bundle=Bundle()
            bundle.putInt(index,position)
            bundle.putParcelableArrayList(list,listItem)

            var frg=   DetailGalleryFrg()
            frg.arguments = bundle

            return frg
        }
    }
}