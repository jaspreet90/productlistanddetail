package com.jaspreet.wolfmanga.ui.home.frg.bottomsheet

import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jaspreet.wolfmanga.R
import com.jaspreet.wolfmanga.repo.network.model.particularcategory.Item
import kotlinx.android.synthetic.main.frg_bottom_sheet_dialog.*
import java.util.ArrayList

class BottomSheetFragment : BottomSheetDialogFragment() {

    var position :Int=0
    var listItem = emptyList<Item>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        getBundleData()
    }

    fun getBundleData(){
        position = arguments?.getInt(index)?:0
        listItem = arguments?.getParcelableArrayList<Item>(list)?: emptyList()
    }


    override fun onCreateView(   inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?  ): View? =
      inflater.inflate(R.layout.frg_bottom_sheet_dialog, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()
    }
    fun setUp(){
        tv_name.text = listItem[position].title
        tv_price.text = requireActivity().getString( R.string.price, listItem[position].price.toString())
    }




    companion object{

        var index ="position"
        var list ="list"

        fun newInstance(  position: Int,  listItem: ArrayList<Item>): BottomSheetFragment{

            Log.d("==pos==", position.toString())

            var bottomSheetFragment = BottomSheetFragment()
            var bundle = Bundle()
            bundle.putInt(index,position)
            bundle.putParcelableArrayList(list,listItem)
            bottomSheetFragment.arguments = bundle

            return bottomSheetFragment
        }

    }

}