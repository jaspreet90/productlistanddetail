package com.jaspreet.wolfmanga.listener

import com.jaspreet.wolfmanga.data.MainCategoryModelUI


interface ItemClick {

   fun onItemClick(mainCategoryModelUI: MainCategoryModelUI)
}