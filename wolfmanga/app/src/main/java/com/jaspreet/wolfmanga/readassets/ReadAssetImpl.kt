package com.jaspreet.wolfmanga.readassets

import android.content.Context
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ReadAssetImpl  @Inject constructor( private  val context: Context) : ReadAsset {
    override fun read(fileName:String) : Flowable<String> {
        var inputStream= context.assets.open(fileName)

      return  Flowable.fromCallable {
            return@fromCallable  inputStream.bufferedReader().use { it.readText() }
        }.observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())

    }
}