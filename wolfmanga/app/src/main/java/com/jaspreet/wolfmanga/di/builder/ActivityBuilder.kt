package com.jaspreet.wolfmanga.di.builder

import com.jaspreet.wolfmanga.ui.home.HomeActivity
import com.jaspreet.wolfmanga.ui.home.frg.detail.DetailProvider
import com.jaspreet.wolfmanga.ui.home.frg.home.HomeProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(HomeProvider::class) , (DetailProvider::class)])
    abstract fun bindHome(): HomeActivity



}