package com.jaspreet.wolfmanga.ui.home
import android.os.Bundle
import android.support.v4.app.Fragment
import com.jaspreet.wolfmanga.R
import com.jaspreet.wolfmanga.ui.base.BaseActivity
import com.jaspreet.wolfmanga.ui.home.frg.detail.DetailFrg
import com.jaspreet.wolfmanga.ui.home.frg.home.HomeFrg
import javax.inject.Inject
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_home.*
import android.view.MenuItem


class HomeActivity : BaseActivity() , HasSupportFragmentInjector {

        @Inject
        lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

        override  fun supportFragmentInjector(): AndroidInjector<Fragment>?  =   fragmentDispatchingAndroidInjector



    override
    fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         setContentView(R.layout.activity_home)
         setSupportActionBar(tool_bar)
         setUp()
    }

    private fun setUp(){
     changeFragment(HomeFrg.newIntance(),false)
    }

  override  fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {

                onBackPressed()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }
}