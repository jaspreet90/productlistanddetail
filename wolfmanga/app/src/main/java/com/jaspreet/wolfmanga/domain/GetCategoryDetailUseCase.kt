package com.jaspreet.wolfmanga.domain

import com.jaspreet.wolfmanga.readassets.ReadAsset
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetCategoryDetailUseCase  @Inject constructor (){

    @Inject lateinit var readAsset: ReadAsset

    fun getDetail()  = readAsset.read("For_a_particular_category.json")
        .observeOn(Schedulers.io())
        .subscribeOn(Schedulers.io())


}