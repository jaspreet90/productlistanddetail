package com.jaspreet.wolfmanga.repo.network.model.maincategory

import com.google.gson.annotations.SerializedName

data class Widget(
    @SerializedName("items")
    val items: List<Item>,
    @SerializedName("title")
    val title: String
)