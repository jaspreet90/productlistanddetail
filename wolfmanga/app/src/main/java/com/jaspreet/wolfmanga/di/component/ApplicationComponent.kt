package com.jaspreet.wolfmanga.di.component


  import com.jaspreet.wolfmanga.WmApplication
  import com.jaspreet.wolfmanga.di.builder.ActivityBuilder
  import com.jaspreet.wolfmanga.di.module.ApplicationModule
 import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidInjectionModule::class), (ApplicationModule::class), (ActivityBuilder::class) ])
interface ApplicationComponent {

    fun inject(app: WmApplication)

}