package com.jaspreet.wolfmanga.ui.home.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.jaspreet.wolfmanga.repo.network.model.particularcategory.Item
import com.jaspreet.wolfmanga.ui.home.frg.detail.DetailGalleryFrg
import java.util.ArrayList

class DetailGalleryPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {


    var listItem: ArrayList<Item> = ArrayList()
        set(value) {
            field=value
            notifyDataSetChanged()
        }

    override fun getCount(): Int {
        return if (listItem==null)
            0
        else
            listItem.size
    }

    override fun getItem(position: Int): Fragment? {
        return DetailGalleryFrg.newInstance(listItem,position)
    }

     override fun getPageTitle(position: Int): CharSequence? {
        return " "
    }
}
