package com.jaspreet.wolfmanga.ui.home.frg.detail

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.jaspreet.wolfmanga.domain.GetCategoryDetailUseCase
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class DetailViewModelFactory @Inject constructor(
    private val getCategoryDetailUseCase: GetCategoryDetailUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailViewModel::class.java)) {

            return DetailViewModel(getCategoryDetailUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}