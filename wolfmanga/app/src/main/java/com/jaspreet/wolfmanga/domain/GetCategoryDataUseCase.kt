package com.jaspreet.wolfmanga.domain

import com.jaspreet.wolfmanga.readassets.ReadAsset
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class GetCategoryDataUseCase @Inject constructor (){

    @Inject lateinit var readAsset: ReadAsset

    fun getList()  = readAsset.read("first.json")
        .observeOn(Schedulers.io())
        .subscribeOn(Schedulers.io())

}