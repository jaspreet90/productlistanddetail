package com.jaspreet.wolfmanga.ui.base

import android.Manifest
import android.support.v4.app.Fragment
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener

abstract class PermissionBaseFragment<out VM : BaseViewModel> : BaseFragment<VM>() {


    override fun hasInjector() = true

    abstract override fun getViewModel(): VM


    fun checkPermission(){
        Dexter.withActivity(requireActivity())
            .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener( object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    permissionGranted()


                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {

                }

            }).check()
    }

    abstract fun permissionGranted()
    abstract fun permissionDenied()
}