package com.jaspreet.wolfmanga.di.module

import android.app.Application
import android.content.Context
import com.jaspreet.wolfmanga.readassets.ReadAsset
import com.jaspreet.wolfmanga.readassets.ReadAssetImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(includes = [])
class ApplicationModule (val app: Application){

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideContext(): Context =app

    @Provides
    @Singleton
    fun provideReadAsset(readAssetImpl: ReadAssetImpl) : ReadAsset = readAssetImpl


}