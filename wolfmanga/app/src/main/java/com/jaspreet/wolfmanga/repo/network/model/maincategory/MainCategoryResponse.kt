package com.jaspreet.wolfmanga.repo.network.model.maincategory

import com.google.gson.annotations.SerializedName

data class MainCategoryResponse(
    @SerializedName("widgets")
    val widgets: List<Widget>
)