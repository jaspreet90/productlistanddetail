package com.jaspreet.wolfmanga.ui.home.frg.detail


import dagger.Module
import dagger.android.ContributesAndroidInjector


    @Module
    abstract class DetailProvider{

        @ContributesAndroidInjector(modules = [(DetailModule::class)])
        abstract fun bindFrg(): DetailFrg

        @ContributesAndroidInjector(modules = [(DetailModule::class)])
        abstract fun bindDetailGalleryFrg(): DetailGalleryFrg


    }